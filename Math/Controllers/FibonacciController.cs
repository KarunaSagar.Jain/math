﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace Math.Controllers
{
    [Route("Math/[controller]")]
    [ApiController]
    public class FibonacciController : ControllerBase
    {
        [HttpGet("{Length}")]
        public string Get(int Length)
        {
            string str;
            int n1 = 0, n2 = 1, n3, i;
            str = n1.ToString() + ", " + n2.ToString();

            for (i = 2; i < Length; ++i)
            {
                n3 = n1 + n2;
                str = str + ", " + n3.ToString();
                n1 = n2;
                n2 = n3;
            }
            return str;
        }
    }
}
