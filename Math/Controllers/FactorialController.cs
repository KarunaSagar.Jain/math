﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace Math.Controllers
{
    [Route("Math/[controller]")]
    [ApiController]
    public class FactorialController : ControllerBase
    {
        [HttpGet("{Number}")]
        public string Get(int Number)
        {
            int i, fact = 1;
            for (i = 1; i <= Number; i++)
            {
                fact = fact * i;
            }
            string str = "Factorial of " + Number + " is: " + fact;
            return str;
        }
    }
}
